/*
Copyright 2024 Johan Maasing <johan@zoom.nu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
use std::fmt::{Display, Formatter};

use serde::{Deserialize, Serialize};
use windows::{
    core::Result,
    Win32::{
        System::Com::{CLSCTX_ALL, CoCreateInstance, CoInitialize},
        UI::Shell::{DesktopWallpaper, IDesktopWallpaper},
    },
};
use windows::core::{HSTRING, PCWSTR, PWSTR};

#[derive(Clone, Deserialize, Serialize, Debug)]
pub struct Wallpaper {
    pub device_id: String,
    pub image_path: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Wallpapers {
    pub wallpapers: Vec<Wallpaper>,
}

impl Display for Wallpaper {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} - {}", self.device_id, self.image_path)
    }
}

impl Display for Wallpapers {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut p = String::new();
        let mut idx = 0;
        for wallpaper in self.wallpapers.clone() {
            p.push_str(format!("({},{}),", idx, wallpaper.image_path.as_str()).as_str());
            idx = idx + 1;
        }
        write!(f, "{}", p)
    }
}

struct DeviceWallpaper {
    device_id_ptr: PWSTR,
    device_id: String,
    image: String,
}

struct DeviceWallPapers {
    desktop: IDesktopWallpaper,
    wallpapers: Vec<DeviceWallpaper>,
}

fn get_desktop_wallpaper_object() -> Result<IDesktopWallpaper> {
    // Initialize COM
    let _ = unsafe { CoInitialize(None) };
    // Create a DesktkopWallpaper object and return its IDesktopWallpaper interface
    unsafe { CoCreateInstance(&DesktopWallpaper, None, CLSCTX_ALL) }
}

fn get_device_wallpapers() -> Result<DeviceWallPapers> {
    let desktop: IDesktopWallpaper = get_desktop_wallpaper_object()?;
    let mut wallpapers: Vec<DeviceWallpaper> = Vec::new();
    unsafe {
        let device_count: u32 = desktop.GetMonitorDevicePathCount()?;
        for device_idx in 0..device_count {
            let device_id_ptr: PWSTR = desktop.GetMonitorDevicePathAt(device_idx)?;
            let image_ptr: PWSTR = desktop.GetWallpaper(PCWSTR(device_id_ptr.0))?;
            wallpapers.push(DeviceWallpaper {
                device_id_ptr,
                device_id: device_id_ptr.to_string()?,
                image: image_ptr.to_string()?,
            })
        }
    }
    Ok(DeviceWallPapers {
        desktop,
        wallpapers,
    })
}

pub fn get_wallpapers() -> Result<Wallpapers> {
    let device_wallpapers = get_device_wallpapers()?;
    let mut wallpapers = Vec::new();
    for wallpaper in device_wallpapers.wallpapers.iter() {
        wallpapers.push(Wallpaper {
            device_id: wallpaper.device_id.clone(),
            image_path: wallpaper.image.clone(),
        });
    }
    return Ok(Wallpapers {
        wallpapers: wallpapers,
    });
}

pub fn set_wallpapers(desired_wallpapers: Wallpapers) -> Result<()> {
    // Check if there are any wallpapers to set
    if desired_wallpapers.wallpapers.is_empty() {
        return Ok(());
    }

    let device_wallpapers = get_device_wallpapers()?;
    for device_wallpaper in device_wallpapers.wallpapers {
        let device_id = device_wallpaper.device_id;
        desired_wallpapers.wallpapers
            .iter()
            .find(|x| { x.device_id == device_id })
            .iter()
            .for_each(|desired_wallpaper: &&Wallpaper| {
                unsafe {
                    let _ = device_wallpapers.desktop.SetWallpaper(
                        PCWSTR(device_wallpaper.device_id_ptr.0),
                        PCWSTR(HSTRING::from(desired_wallpaper.image_path.clone()).as_ptr()),
                    );
                };
            });
    }

    Ok(())
}

