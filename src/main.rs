/*
Copyright 2024 Johan Maasing <johan@zoom.nu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
use fs::{read, write};
use std::fs;

use anyhow::Result;
use clap::{Parser, Subcommand};

mod wallpaper;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Load a wallpaper configuration from the named configuration and replace the current windows wallpapers.
    Load {
        /// The configuration file to apply
        file: String,
    },
    /// Save the current windows wallpapers to the named configuration
    Save {
        /// The configuration file to write. This function will create a file if it does not exist, and will entirely replace its contents if it does.
        file: String,
    },
}

fn main() -> Result<()> {
    let cli: Cli = Cli::parse();
    match &cli.command {
        Commands::Save { file } => {
            let wp = wallpaper::get_wallpapers()?;
            let json = serde_json::to_string_pretty(&wp).expect("Unable to serialize to JSON");
            write(file, json)?;
        }
        Commands::Load { file} => {
            let content = read(file)?;
            let wp: wallpaper::Wallpapers = serde_json::from_slice(content.as_slice())?;
            wallpaper::set_wallpapers(wp)?;
        }
    };
    anyhow::Ok(())
}
