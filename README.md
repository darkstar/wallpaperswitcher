# wallpaperswitcher

Is a small utility to save the paths of the current background wallpaper images and to restore them.
I use this because I often want to set background images on several displays at the same time and it's pretty hard to do with the default windows wallpaper UI.

## Save current settings
```
PS C:\> wallpaperswitcher.exe save foo.json
PS C:\> type .\foo.json
{
  "wallpapers": [
    {
      "device_id": "\\\\?\\DISPLAY#...",
      "image_path": "c:\\...\\foo.png"
    },
    {
      "device_id": "\\\\?\\DISPLAY#...",
      "image_path": "C:\\...Temp.png"
    },
    {
      "device_id": "\\\\?\\DISPLAY#...",
      "image_path": "c:\\...\\bar.jpg"
    }
  ]
}
```

The device_id is the windows ID for the particular monitor. If you switch to another monitor (change the hardware) the device ID will be different. 

## Load from file
```
PS C:\> wallpaperswitcher.exe load .\foo.json
```

Sets the wallpapers from the file.

## Notes

There are no validations performed that images exists et c. This code simply calls to the windows API to set the path of the background image. If the image does not exists you are at the mercy of the windows error handling.

## License

http://www.apache.org/licenses/LICENSE-2.0